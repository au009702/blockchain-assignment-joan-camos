﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

//static variable for the diff

namespace BlockchainAssignment
{
    internal class Block
    {
        public int index;
        DateTime timestamp;
        public String hash;
        public String prevHash;

        public List<Transaction> transactionList = new List<Transaction>();

        public String merkleRoot;


        //nonce = one time token given
        //proof of work
        public long nonce = 0;
        public int difficulty = 4;

        //
        public long enonce = 0;


        //rewards and fees
        public double reward = 1.0; //fixed logic 
        public double fees = 0.0;


        public String minerAddress = String.Empty;
        public int threadsCount = 4;
        public bool IsComplete;
        public long elapsedTime;
        public Stopwatch stopwatch;
        public String finalhash;



        // public int threadsCount;
        // public object stopwatch;
        // public bool IsComplete;
        // public object elapsedTime;
        //  public string finalHash;
        //  public object _nonceLock;


        /*Genesis Block Constructor*/
        public Block()
        {
            this.timestamp = DateTime.Now;
            this.index = 0;
            this.prevHash = String.Empty;
            reward = 0;
           this.merkleRoot = MerkleRoot(transactionList);
            this.hash = Mine();
        }

        public Block(int index, String hash)
        {
            this.timestamp = DateTime.Now;
            this.index = index + 1;
            this.prevHash = hash;
            this.hash = CreateHash();
        }


        public Block(Block lastBlock, List<Transaction> transactions, String address = "")
        {
            this.timestamp = DateTime.Now;
            this.index = lastBlock.index + 1;
            this.prevHash = lastBlock.hash;

            minerAddress = address;

            transactions.Add(CreateRewardTransaction(transactions));
            transactionList = transactions;


            merkleRoot = MerkleRoot(transactionList);


            this.hash = Mine();
        }

        public Transaction CreateRewardTransaction(List<Transaction> transactions)
        {
            
            fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee);
            
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, "");


            //SUmm the fees in the list  of transactions in the mined block
            // Create the "Reward Transaction" being the sum of fees and reward being transferred from "Mine Rewards" (coin base) to miner
        }

        public String CreateHash()
        {
            SHA256 hasher = SHA256Managed.Create();


            //concatinate all blocks  //
            String input = index.ToString() + timestamp.ToString() + prevHash + nonce.ToString() + reward.ToString() + merkleRoot;
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            String hash = string.Empty;

            //Covert Hash from byte array to String
            foreach (byte x in hashByte)
            {
                hash += String.Format("{0:x2}", x);
            }

            return hash;

        }


        //This method wraps the Mine method in a task and runs it asynchronously. The MineAsync method returns a Task that can be awaited to retrieve the final hash value.

        // Add a new method for multithreading
        //public async Task<String> MineAsync()
        //    {
        //        return await Task.Run(() => Mine());
        //    }







        public String Mine() //initiates mining process
        {
            String hash = CreateHash(); //we create the initial hash
            Thread[] threads = new Thread[threadsCount]; //nefining array of threads
            IsComplete = false; //process will be completed when this variable = true


        //Difficutly criteria
        String re = new string('0', difficulty); //create a regex string of N (difficulty ie 4) 0's


        stopwatch = Stopwatch.StartNew(); //create stopwatch to measure the time it takes


            //re-hash until difficulty criteria is met
            for (int i = 0; i < threadsCount; i++)
        {
            ThreadStart threadDelegate = new ThreadStart(() => MineThread(i, re, hash));
            threads[i] = new Thread(threadDelegate);
            Console.WriteLine("Thread " + i + " initiated");
            threads[i].Start();
        }

        
            while (!IsComplete) { }
            stopwatch.Stop(); //this will stop the timer when the hash is being found

            elapsedTime = stopwatch.ElapsedMilliseconds; //how much time it took

            Console.WriteLine("Hash " + finalhash + " found in " + elapsedTime + "ms"); //outputs final time and elapsedtime of the threads
            return finalhash;
        }


        public void MineThread(int threadNumber, string re, string hash) //arguments being passed
        {
            long threadN; 

            while (!hash.StartsWith(re)) //calculates till it gets the difficulty requirements
            {
                if (IsComplete) return;
                {
                    threadN = nonce++; //generates a new nonce that increments every iteration
                }
                hash = CreateHash();
            }
            IsComplete = true; //finish
            finalhash = hash;
        }






        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList();
            if (hashes.Count == 0)
            {
                return String.Empty;
            }
            if (hashes.Count == 1)
            {
                return HashCode.HashTools.CombineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1)
            {
                List<String> merkleLeaves = new List<String>();
                for (int i = 0; i < hashes.Count; i += 2)
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i]));
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i + 1]));
                    }
                }
                hashes = merkleLeaves;
            }
            return hashes[0];
        }

        public override string ToString()
        {

            String output = String.Empty;
            transactionList.ForEach(t => output += (t.ToString() + "\n"));



            return "Index: " + index.ToString()
                + "\nTimestamp: " + timestamp.ToString()
                + "\nPrevious Hash: " + prevHash
                + "\nHash: " + hash +
                "\nNonce: " + nonce.ToString() +
                "\nDifficulty: " + difficulty.ToString() +
                "\nReward: " + reward.ToString() +
                "\nFees: " + fees.ToString() +
                "\nMiner's Address: " + minerAddress +
                "\nTransactions:\n" + output + "\n";
        }
    }
}
