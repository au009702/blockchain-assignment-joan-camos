﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlockchainAssignment
{
    public partial class BlockchainApp : Form
    {
        Blockchain blockchain;

        public BlockchainApp()
        {
            InitializeComponent();
            blockchain = new Blockchain();
            richTextBox1.Text = "New Blockchain Initialised!";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = 0;
            if (Int32.TryParse(textBox1.Text, out index))
            {
                richTextBox1.Text = blockchain.GetBlockAsString(index);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void GenerateWallet_Click(object sender, EventArgs e)
        {
            String privKey;
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out privKey);
            this.publicKey.Text = myNewWallet.publicID;
            privateKey.Text = privKey;
            String publicKey = myNewWallet.publicID;
            Console.WriteLine(publicKey + "\n" + privKey);
        }

        private void button3_Click(object sender, EventArgs e)
        {

            if (Wallet.Wallet.ValidatePrivateKey(privateKey.Text, publicKey.Text))
            {
                richTextBox1.Text = "Keys are Valid";
            }
            else
            {
                richTextBox1.Text = "Keys are Invalid";
            }

        }

        private void createTransaction_Click(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction(publicKey.Text, receiverKey.Text, Double.Parse(amount.Text), Double.Parse(fee.Text), privateKey.Text);
            blockchain.transactionPool.Add(transaction);
            richTextBox1.Text = transaction.ToString();
        }

        private void newBlock_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.getPendingTransactions();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, publicKey.Text);
            blockchain.Blocks.Add(newBlock);

            richTextBox1.Text = blockchain.ToString();
        }



        private void validateChain_Click(object sender, EventArgs e)
        {
            // Contiguity Checks
            bool valid = true;

            if (blockchain.Blocks.Count == 1)
            {
                //
                if (!blockchain.validateMerkleRoot(blockchain.Blocks[0]))
                {
                    richTextBox1.Text = "Blockchain is invalid";

                }
                else
                {
                    richTextBox1.Text = "Blockchain is valid";
                }

                return;
            }
            for (int i = 1; i < blockchain.Blocks.Count; i++)
            {
                // Compare Neighbouring hash/previous hash for all blocks in the blockchain
                if (blockchain.Blocks[i].prevHash != blockchain.Blocks[i - 1].hash || !blockchain.validateMerkleRoot(blockchain.Blocks[i]))
                {
                    richTextBox1.Text = "Blockchain is invalid";
                    return;

                }

            }

            if (valid)
            {
                richTextBox1.Text = "Blockchain is valid";
            }
            else
            {
                richTextBox1.Text = "Blockchain is invalid";
            }
        }

        private void checkBalance_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = blockchain.GetBalance(publicKey.Text).ToString() + " Assignment Coin";
        }

   

        // Print entire blockchain to UI
        //private void ReadAll_Click(object sender, EventArgs e)
        //{
        //    UpdateText(blockchain.ToString());
        //}

        // Print Block N (based on user input)
        //private void PrintBlock_Click(object sender, EventArgs e)
        //{
        //    if (Int32.TryParse(blockNo.Text, out int index))
        //        UpdateText(blockchain.GetBlockAsString(index));
        //    else
        //        UpdateText("Invalid Block No.");
        //}

        // Print pending transactions from the transaction pool to the UI
        private void ReadPendingTransactions_Click(object sender, EventArgs e)
        {
            String txtOutput = String.Empty;
            blockchain.transactionPool.ForEach(t => txtOutput += (t.ToString() + "\n"));
            richTextBox1.Text = txtOutput;
        }

    }
}